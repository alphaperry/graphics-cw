// camera.h

#pragma once

#include <vector>

class CCamera
{
public:
    float m_f;
    float m_R[3][3];
    float m_t[3];
};

void ReadCameras(std::vector<CCamera> &cameras, const char* filename);
void WriteCameras(std::vector<CCamera> &cameras, const char* filename);
