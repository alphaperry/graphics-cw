// ppm.cpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ReadPPM(unsigned char* &data, int &nRows, int &nCols, const char* filename)
{
    int maxComments = 10;

    FILE* fptr = fopen(filename, "r");

    char buff[200];
    char *cret = fgets(buff, 3, fptr);

    if (!strcmp(buff, "P6"))
    {
        // read comments until we see a number in the range 0-9     
        int nComment = 0;
        while ((buff[0] < 48 || buff[0] > 57) && (nComment < maxComments)) 
        {
            cret = fgets(buff, 200, fptr);
            nComment++;
        }

        sscanf(buff, "%d %d\n", &nCols, &nRows);        

        int maxVal;
        cret = fgets(buff, 200, fptr);
        sscanf(buff, "%d\n", &maxVal);
        
        int nBands = 3;
        int nData = nRows * nCols * nBands;
        data = new unsigned char[nData];
        int iret = fread(data, 1, nData, fptr);
    }
    else
    {
        printf("ReadPPM -- cannot read PPM file\n");        
    }    
    fclose(fptr);
}

void WritePPM(unsigned char* data, int nRows, int nCols, const char* filename)
{
    FILE* fptr = fopen(filename, "w");
    fprintf(fptr, "P6\n");
    fprintf(fptr, "%d %d\n", nCols, nRows);
    fprintf(fptr, "255\n");
    int nBands = 3;
    int nData = nRows * nCols * nBands;
    fwrite(data, 1, nData, fptr);
    fclose(fptr);
}
