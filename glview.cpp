// glview.cpp

////////////////////////////////////////////////////////////
//
// To get started, search for the sections marked "TODO"
//
////////////////////////////////////////////////////////////

// Includes
#include "ppm.h"
#include "ply.h"
#include "camera.h"
#include "matrix.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <string>

using namespace std;

// LINUX
#ifdef __linux__
#include "GL/glut.h"
#endif

// MAC
#ifdef __APPLE__
#include "GLUT/glut.h"
#endif

// WINDOWS
#ifdef _WIN32
#include "glut.h"
#endif

// Global variables (prefixed with g_, needed for GLUT callbacks)

// data directory
string g_dataDir = "cm20219_data";
//string g_dataDir = "c:/cm20219_data"; // put data on local drive for faster loading
string g_imageDir = g_dataDir;

// input key states
bool g_keyLeft = false;
bool g_keyRight = false;
bool g_keyUp = false;
bool g_keyDown = false;
bool g_keyPlus = false;
bool g_keyMinus = false;

// fills triangles if false
bool g_wireframe = false;
bool g_normalsEnabled = true;

// material params
GLfloat g_refl_amb[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat g_refl_diff[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat g_refl_spec[] = { 1.0, 1.0, 1.0, 1.0 };
float g_shininess = 50;

// light params
GLfloat g_light_amb[] = { 0.2, 0.2, 0.2, 1 };
GLfloat g_light_diff[] = { 0.5, 0.5, 0.5, 1 };
GLfloat g_light_spec[] = { 0.6, 0.6, 0.6, 1 };

// view parameters
float g_theta = 93.0f;
float g_dtheta = 1.0f;
float g_phi = 140.0f;
float g_dphi = 1.0f;
float g_fovy = 50.0;
float g_tz = -3.0f;
float g_dt = 0.02f;
float g_tzMax = -1.0f;
float g_tzMin = -20.0f;
float g_zNear = 0.1f;
float g_zFar = 1000.0f;
float g_ptSz = 1.0f;
float g_viewportWidth = 640;
float g_viewportHeight = 480;

// 3D model params
GLuint *g_textureID; // GL texture handles

GLuint *g_textures; // maps triangles to textures. i.e. texture = g_textures[triangle]

float **g_points; // g_nPoints      * 3 matrix containing 3D positions of points
int **g_triangles; // g_nTriangles  * 3 matrix containing point IDs of each triangle
float **g_normals; // g_nTriangles  * 3 matrix containing normal vectors of each triangle
float **g_uvs; // g_nTriangles      * 3 matrix containing uvs for each point

int g_nPoints; // number of 3D points
int g_nTriangles; // number of triangles
int g_nImages; // number of texture images

float g_mn[3]; // mean of 3D points

bool g_showTeapot = false; // draw teapot for testing purposes

// Geometry and texture processing

void GenerateTexture(const char *filename, int textureID);
void ComputeNormals(float** normals, float** points, int** triangles,
		int nPoints, int nTriangles);
void CalculateMean(float mn[3], float** points, int nPoints);
void DrawLine(float x[3], float y[3], float r, float g, float b);

// GLUT callbacks

void display(void);
void init(int argc, char** argv);
void reshape(int w, int h);
void idle(void);
void keyboard_s(int key, int x, int y);
void keyboard_su(int key, int x, int y);
void keyboard(unsigned char key, int x, int y);
void keyboard_u(unsigned char key, int x, int y);

// Main

int main(int argc, char** argv) {
	// Create GLUT view
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(g_viewportWidth, g_viewportHeight);
	glutCreateWindow("glview");

	// Register callbacks
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboard_u);
	glutSpecialFunc(keyboard_s);
	glutSpecialUpFunc(keyboard_su);
	glutDisplayFunc(display);
	glutIdleFunc(idle);

	printf("%s \n", glGetString(GL_VENDOR));

	// Initialise and launch GLUT
	init(argc, argv);
	glutMainLoop();

	// Delete storage
	DeleteMatrix(g_points, g_nPoints);
	DeleteMatrix(g_triangles, g_nTriangles);
	DeleteMatrix(g_normals, g_nTriangles);
	delete[] g_textureID;
}

// GLUT callbacks

void init(int argc, char** argv) {
	// Set lighting and material properties
	glClearColor(0.0, 0.0, 0.0, 0.0);

	GLfloat light_position[] = { 10, 10, 5, 0 };
	glEnable(GL_LIGHT0);

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, g_light_amb);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, g_light_diff);
	glLightfv(GL_LIGHT0, GL_SPECULAR, g_light_spec);

	glMaterialfv(GL_FRONT, GL_AMBIENT, g_refl_amb);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, g_refl_diff);
	glMaterialfv(GL_FRONT, GL_SPECULAR, g_refl_spec);
	glMateriali(GL_FRONT, GL_SHININESS, g_shininess);

	glShadeModel(GL_SMOOTH);
	glPolygonMode(GL_FRONT, GL_FILL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);
	glPointSize(g_ptSz);
	glEnable(GL_POINT_SMOOTH);

	// Read mesh
	string plyFilename = g_dataDir + "/mac.ply";
	ReadPLY(g_points, g_triangles, g_nPoints, g_nTriangles,
			plyFilename.c_str());

	// Read cameras
	vector<CCamera> cameras;
	string camFilename = g_dataDir + "/mac.cam";
	ReadCameras(cameras, camFilename.c_str());
	g_nImages = cameras.size();

	// Compute normals
	NewMatrix(g_normals, g_nTriangles, 3);
	ComputeNormals(g_normals, g_points, g_triangles, g_nPoints, g_nTriangles);

	// Load texture images
	g_textureID = new GLuint[g_nImages];
	glGenTextures(g_nImages, g_textureID);

  // generate textures for each image
	for (int i = 0; i < g_nImages; i++) {
		char filename[100];
		printf(".");
		sprintf(filename, "%s/%d%d.ppm", g_imageDir.c_str(), i / 10, i % 10);
		GenerateTexture(filename, g_textureID[i]);
	}

  NewMatrix(g_uvs, g_nPoints, 3);
  int cam = 0, f = 0;

  g_textures = new GLuint[g_nTriangles];

  float imageProj[3][3] = { {+f, 0, 240.0f}, {0, -f, 135.0f}, {0, 0, 1.0f} };
  float textCoords[3] = {0,0,0};
  float dot;
  float camRot[3][3]; // camera rotation transposed
  float *camForward; // forward vector of camera
  float z[3] = {0,0,1.0f};

  for(int t = 0; t < g_nTriangles; t++)
  {
    float lowest_dot = 10;
    for(int c = 0; c < g_nImages; c++)
    {
       Transpose3x3(camRot, cameras.at(c).m_R);
       Multiply3x3(camForward, camRot, z);
       //printf("(%f,%f,%f);", camForward[0], camForward[1], camForward[2]);
       //printf("(%f,%f,%f);", g_normals[t][0], g_normals[t][1], g_normals[t][2]);

       //dot = Dot3(g_normals[t], camForward);
       /*if(dot < lowest_dot)
       {
          lowest_dot = dot;
          cam = c;
       }*/
    }
    g_textures[t] = cam;
    for(int point = g_triangles[t][0], count = 0; count < 3; count++, point = g_triangles[t][count])
    {
      // X_c = RX + t
      Multiply3x3(textCoords, cameras.at(cam).m_R, g_points[point]);
      Add3(textCoords, textCoords, cameras.at(cam).m_t);

      // set focal point in texture projection matrix
      f = cameras.at(cam).m_f;
      imageProj[0][0] = +f;
      imageProj[1][1] = -f;
      
      /*Print3(imageProj[0]);
      Print3(imageProj[1]);oo
      Print3(imageProj[2]);
      Print3(textCoords);*/
      
      // project
      Multiply3x3(g_uvs[point], imageProj, textCoords);
      //Print3(g_uvs[i]); 
      Div3(g_uvs[point], g_uvs[point], g_uvs[point][2]);
      // normalize by texture size
      Div3(g_uvs[point], g_uvs[point], 512.0f);
      Print3(g_uvs[point]); 
    }

  }

	/////////////////////////////////////////////
	//
	// TODO: compute texture coordinates
	//
	/////////////////////////////////////////////

	CalculateMean(g_mn, g_points, g_nPoints);
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_SMOOTH);

	// Initialise projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(g_fovy, g_viewportWidth / g_viewportHeight, g_zNear, g_zFar);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	/*float x1[3] = { 0.0f, 0.0f, 0.0f };
	float x2[3] = { 5.0f, 0.0f, 0.0f };
	DrawLine(x1, x2, 0.3f, 0.3f, 0.4f);*/

	// Set model pose
	glTranslatef(0, 0, g_tz);
  // rotate about y axis by theta
	glRotatef(g_theta,0, 1, 0);
  // rotate about x axis by phi
	glRotatef(g_phi,   1, 0, 0);
  //printf("(%f,%f)", g_theta, g_phi);

	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);

  // enabled/disables filling of triangles 
  glPolygonMode(GL_FRONT_AND_BACK, (g_wireframe ? GL_LINE : GL_FILL));

	if (g_showTeapot) {
		// Show teapot for testing
		glutSolidTeapot(0.4);
	} else {

		glTranslatef(-g_mn[0], -g_mn[1], -g_mn[2]);

    // loop of triangle list
		for (int t = 0; t < g_nTriangles; t++)
    {
      glBindTexture(GL_TEXTURE_2D, g_textureID[0]);
   		glBegin(GL_TRIANGLES);
      if(g_normalsEnabled)
        glNormal3f(g_normals[t][0], g_normals[t][1], g_normals[t][2]);
      for(int j = 0; j < 3; j++)
      {

        int tri = g_triangles[t][j];
        // mapping just one texture.
        glTexCoord2f(g_uvs[tri][0], g_uvs[tri][1]);
        glVertex3f(g_points[tri][0], g_points[tri][1], g_points[tri][2]);
      }
    		glEnd();
    }

	}

	glFlush();
	glutSwapBuffers();
}

void reshape(int w, int h) {
	glViewport(0, 0, w, h);
}

void idle(void) {
	if (g_keyLeft || g_keyRight || g_keyUp || g_keyDown || g_keyPlus || g_keyMinus) {
		if (g_keyLeft)
			g_theta -= g_dtheta;
		if (g_keyRight)
			g_theta += g_dtheta;

		if (g_theta > 360)
			g_theta = 0;
		if (g_theta < 0)
			g_theta = 360;

    if(g_keyUp)
      g_phi -= g_dphi;
    if(g_keyDown)
      g_phi += g_dphi;

		if (g_phi > 360)
			g_phi = 0;
		if (g_phi < 0)
			g_phi = 360;

		if (g_keyPlus)
			g_tz += g_dt;
		if (g_keyMinus)
			g_tz -= g_dt;

		g_tz = min(g_tzMax, max(g_tz, g_tzMin));

		glutPostRedisplay();
	}
}

// keypress handlers

void keyboard_s(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_LEFT:
		g_keyLeft = true;
		g_keyRight = false;
		break;
	case GLUT_KEY_RIGHT:
		g_keyRight = true;
		g_keyLeft = false;
		break;
	case GLUT_KEY_UP:
		g_keyUp = true;
		g_keyDown = false;
		break;
	case GLUT_KEY_DOWN:
		g_keyDown = true;
		g_keyUp = false;
		break;
	}
}

void keyboard_su(int key, int x, int y) {
	switch (key) {
	case GLUT_KEY_LEFT:
		g_keyLeft = false;
		break;
	case GLUT_KEY_RIGHT:
		g_keyRight = false;
		break;
	case GLUT_KEY_UP:
		g_keyUp = false;
		break;
	case GLUT_KEY_DOWN:
		g_keyDown = false;
		break;
	}
}

void keyboard(unsigned char key, int x, int y) {
	switch (key) {
	case 0x1b:
		exit(0);
		break;
	case '+':
	case '=':
		g_keyPlus = true;
		g_keyMinus = false;
		break;
	case '-':
		g_keyMinus = true;
		g_keyPlus = false;
		break;
	}
}

void keyboard_u(unsigned char key, int x, int y) {
	switch (key) {
	case ' ':
		g_showTeapot = !g_showTeapot;
		break;
  case 'q':
  case 'Q':
    g_wireframe = !g_wireframe;
    break;
  case 'e':
  case 'E':
    g_normalsEnabled = !g_normalsEnabled;
    break;
	case '+':
	case '=':
		g_keyPlus = false;
		break;
	case '-':
		g_keyMinus = false;
		break;
	}

	glutPostRedisplay();
}

// Geometry and texture processing

void GenerateTexture(const char *filename, int textureID) {
	unsigned char* data;
	int nRows, nCols;
	ReadPPM(data, nRows, nCols, filename);

	// Flip texture vertically
	// (note y = 0 row is the first row in memory for OpenGL textures)

	int nData = nRows * nCols * 3;
	int nRowBytes = nCols * 3;
	unsigned char* rowPtr0 = data;
	unsigned char* rowPtr1 = data + nData - nRowBytes;
	unsigned char* rowBuffer = new unsigned char[nRowBytes];

	for (int i = 0; i < nRows / 2; i++) {
		memcpy(rowBuffer, rowPtr0, nRowBytes);
		memcpy(rowPtr0, rowPtr1, nRowBytes);
		memcpy(rowPtr1, rowBuffer, nRowBytes);
		rowPtr0 += nRowBytes;
		rowPtr1 -= nRowBytes;
	}

	delete rowBuffer;

	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
			GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_NEAREST);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, nCols, nRows, GL_RGB, GL_UNSIGNED_BYTE,
			data);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	delete data;
}

void ComputeNormals(float** normals, float** points, int** triangles,
		int nPoints, int nTriangles) {
	// Compute normals for each triangle

	// normals[i]    = normal of ith triangle
	// normals[i][j] = jth coordinate of normal (0=x,1=y,2=z)

  for(int i = 0; i < g_nTriangles; i++)
  {
    float **normal, **u, **v;
    NewMatrix(normal, 3, 1);
    NewMatrix(u, 3, 1);
    NewMatrix(v, 3, 1);

    // find two vectors
    Sub3(u[0], points[triangles[i][0]], points[triangles[i][2]]);
    Sub3(v[0], points[triangles[i][0]], points[triangles[i][1]]);
    // find vector parallel to u and v
    Cross3(normal[0], u[0], v[0]);
    // normalise vector (|v| = 1)
    Div3(normal[0], normal[0], Norm3(normal[0]));

    for(int x = 0; x < 3; x++)
      normals[i][x] = normal[0][x]; 
  }

}

void CalculateMean(float mn[3], float** points, int nPoints) {
	for (int i = 0; i < g_nPoints; i++)
		Add3(mn, mn, points[i]);
	Div3(mn, mn, g_nPoints);
	// approximate mean for now
	//mn[0] = 0; mn[1] = 3; mn[2] = -4;
}

void DrawLine(float x[3], float y[3], float r, float g, float b) {
	glBegin(GL_LINES);
    glLineWidth(0.8f);
	  glColor3f(r, g, b);
  	glVertex3f(x[0], x[1], x[3]);
	  glVertex3f(y[0], y[1], y[2]);
	glEnd();
}
