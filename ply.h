// ply.h

#pragma once

void ReadPLY(float** &points, int** &triangles, int &nPoints, int &nTriangles, const char* filename);
