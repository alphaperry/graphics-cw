// ply.cpp

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "matrix.h"

void ReadPLY(float** &points, int** &triangles, int &nPoints, int &nTriangles, const char* filename)
{
    FILE* fptr = fopen(filename, "r");
    
    const int buffSize(200);
    char buff[buffSize];
    char buff1[buffSize];
    char buff2[buffSize];
    char *cret = fgets(buff, 4, fptr);
    
    if (!strcmp(buff, "ply"))
    {
        while (strncmp(buff, "end", 3))
        {
            fgets(buff, buffSize, fptr);
            sscanf(buff, "%s", buff1);

            if (!strcmp(buff1, "element"))
            {
                int i;
                sscanf(buff, "%s %s %d", buff1, buff2, &i);
                if (!strcmp(buff2, "vertex"))
                {
                    nPoints = i;
                    printf("nPoints = %d\n", nPoints);
                }
                if (!strcmp(buff2, "face"))
                {
                    nTriangles = i;
                    printf("nTriangles = %d\n", nTriangles);
                }
            }        
        }  

        // Read points
        NewMatrix(points, nPoints, 3);

        for (int i = 0; i < nPoints; i++)
        {
            fgets(buff, buffSize, fptr);
            float x, y, z;
            sscanf(buff, "%f %f %f", &x, &y, &z);
            points[i][0] = x;
            points[i][1] = y;
            points[i][2] = z;
        }
        
        // Read triangles
        
        NewMatrix(triangles, nTriangles, 3);
        for (int i = 0; i < nTriangles; i++)
        {
            fgets(buff, buffSize, fptr);
            int dum, t1, t2, t3;
            sscanf(buff, "%d %d %d %d", &dum, &t1, &t2, &t3);
            triangles[i][0] = t1;
            triangles[i][1] = t2;
            triangles[i][2] = t3;            
        }
    }
    else
    {
        printf("ReadPLY -- cannot read ply file\n");
    }
    fclose(fptr);
}

    

