// matrix.h

#pragma once

template <class T> void NewMatrix(T** &M, int nRows, int nCols); // Allocate new matrix
template <class T> void DeleteMatrix(T** &M, int nRows);         // Delete matrix

void Copy3(float y[3], float x[3]);                      // Copy 3 vectors (y = x)

void Add3(float y[3], float x1[3], float x2[3]);         // Sum of 3 vectors (y = x1 + x2)
void Sub3(float y[3], float x1[3], float x2[3]);         // Subtract 3 vectors (y = x1 - x2)
void Mult3(float y[3], float x[3], float s);             // Multiply 3 vector by scalar 
void Div3(float y[3], float x[3], float s);              // Divide 3 vector by scalar

float Dot3(float x1[3], float x2[3]);                    // Dot product of 3 vectors (returns x1.x2)
void Cross3(float y[3], float x1[3], float x2[3]);       // Cross product of 3 vectors 
void Outer3(float M[3][3], float x1[3], float x2[3]);    // Outer product of 3 vectors
float Norm3(float x[3]);                                 // Norm of a 3 vector

void Multiply3x3(float y[3], float M[3][3], float x[3]); // Multiply 3 vector by 3x3 matrix (y = Mx)
void Inverse3x3(float Minv[3][3], float M[3][3]);        // Invert 3x3 matrix
void Transpose3x3(float Mt[3][3], float M[3][3]);        // Transpose 3x3 matrix

void Print3(float y[3]);                                 // Output to standard out

// template implementations

template <class T>
void NewMatrix(T** &M, int nRows, int nCols)
{
    M = new T*[nRows];
    for (int i = 0; i < nRows; i++)
        M[i] = new T[nCols];
}

template <class T>
void DeleteMatrix(T** &M, int nRows)
{
    for (int i = 0; i < nRows; i++)
        delete[] M[i];
    delete[] M;
}

