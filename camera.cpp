// camera.cpp

#include "camera.h"

#include <stdio.h>

void ReadCameras(std::vector<CCamera> &cameras, const char* filename)
{
    FILE* fptr = fopen(filename, "r");
    
    const int buffSize = 200;
    char buff[buffSize];
    char* sret;

    sret = fgets(buff, buffSize, fptr);
    
    int nCameras;
    sscanf(buff, "%d", &nCameras);
    cameras.resize(nCameras);
    
    for (int i = 0; i < nCameras; i++)
    {
        sret = fgets(buff, buffSize, fptr);
        sscanf(buff, "%f", &cameras[i].m_f);
        
        sret = fgets(buff, buffSize, fptr);
        sscanf(buff, "%f %f %f", &cameras[i].m_R[0][0], &cameras[i].m_R[0][1], &cameras[i].m_R[0][2]);
        sret = fgets(buff, buffSize, fptr);
        sscanf(buff, "%f %f %f", &cameras[i].m_R[1][0], &cameras[i].m_R[1][1], &cameras[i].m_R[1][2]);
        sret = fgets(buff, buffSize, fptr);
        sscanf(buff, "%f %f %f", &cameras[i].m_R[2][0], &cameras[i].m_R[2][1], &cameras[i].m_R[2][2]);
        
        sret = fgets(buff, buffSize, fptr);
        sscanf(buff, "%f %f %f", &cameras[i].m_t[0], &cameras[i].m_t[1], &cameras[i].m_t[2]); 
    }
        
    fclose(fptr);
}

void WriteCameras(std::vector<CCamera> &cameras, const char* filename)
{
    FILE* fptr = fopen(filename, "w");
    int nCameras = cameras.size();

    fprintf(fptr, "%d\n", nCameras);
    for (int i = 0; i < nCameras; i++)
    {
        fprintf(fptr, "%f\n", cameras[i].m_f);
        for (int j = 0; j < 3; j++)
        {
            for (int k = 0; k < 3; k++)
                fprintf(fptr, "%f ", cameras[i].m_R[j][k]);
            fprintf(fptr, "\n");
        }
        for (int j = 0; j < 3; j++)
            fprintf(fptr, "%f ", cameras[i].m_t[j]);
        fprintf(fptr, "\n");
    }

    fclose(fptr);
}
            
