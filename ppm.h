// ppm.h

#pragma once

void ReadPPM(unsigned char* &data, int &nRows, int &nCols, const char* filename);
void WritePPM(unsigned char* data, int nRows, int nCols, const char* filename);
