TGTS=glview

UNAME:=$(shell uname)
# MAC
ifeq ($(UNAME), Darwin) 
LIBS=-framework GLUT -framework OpenGL
endif
# LINUX
ifeq ($(UNAME), Linux)
LIBS=-lGL -lGLU -lglut 
endif

OBJS=ply.o ppm.o camera.o matrix.o

all:$(TGTS)

glview:glview.cpp $(OBJS)
	g++ -o glview glview.cpp $(OBJS) $(LIBS)

%.o:%.cpp
	g++ -c $<

clean:
	rm -r -f $(TGTS) *.o *~ *.dSYM
