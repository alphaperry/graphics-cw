// matrix.cpp

#include <string.h>
#include <math.h>
#include <stdio.h>

void Copy3(float y[3], float x[3])
{
    for (int i = 0; i < 3; i++)
        y[i] = x[i];
}

void Add3(float y[3], float x1[3], float x2[3])
{
    for (int i = 0; i < 3; i++)
        y[i] = x1[i] + x2[i];
}

void Sub3(float y[3], float x1[3], float x2[3])
{
    for (int i = 0; i < 3; i++)
        y[i] = x1[i] - x2[i];
}

void Mult3(float y[3], float x[3], float s)
{
    for (int i = 0; i < 3; i++)
        y[i] = x[i] * s;
}

void Div3(float y[3], float x[3], float s)
{
    for (int i = 0; i < 3; i++)
        y[i] = x[i] / s;
}

float Dot3(float x1[3], float x2[3])
{
    float ret = 0;
    for (int i = 0; i < 3; i++)
        ret += x1[i] * x2[i];
    return ret;
}

void Cross3(float y[3], float x1[3], float x2[3])
{
    y[0] = x1[1]*x2[2] - x2[1]*x1[2];
    y[1] = x1[2]*x2[0] - x2[2]*x1[0];
    y[2] = x1[0]*x2[1] - x2[0]*x1[1];
}

void Outer3(float M[3][3], float x1[3], float x2[3])
{
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            M[i][j] = x1[i] * x2[j];
}

float Norm3(float x[3])
{
    return sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
}

void Multiply3x3(float y[3], float M[3][3], float x[3])
{
    memset(y, 0, 3*sizeof(float));
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            y[i] += M[i][j] * x[j];
}    

void Inverse3x3(float Minv[3][3], float M[3][3])
{
    float a = M[0][0]; float b = M[0][1]; float c = M[0][2];
    float d = M[1][0]; float e = M[1][1]; float f = M[1][2];
    float g = M[2][0]; float h = M[2][1]; float i = M[2][2];
    
    Minv[0][0] = e * i - f * h;
    Minv[1][0] = - (d * i - f * g);
    Minv[2][0] = d * h - e * g;
    Minv[0][1] = - (b * i - c * h);
    Minv[1][1] = a * i - c * g;
    Minv[2][1] = - (a * h - b * g);
    Minv[0][2] = b * f - c * e;
    Minv[1][2] = - (a * f - c * d);
    Minv[2][2] = a * e - b * d;        

    float det = a * e * i + b * f * g + c * d * h - c * e * g - a * f * h - b * d * i;
 
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            Minv[i][j] /= det;   
}

void Transpose3x3(float Mt[3][3], float M[3][3])
{
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            Mt[i][j] = M[j][i];   
}

void Print3(float x[3])
{
    for (int i = 0; i < 3; i++)
        printf("%f ", x[i]);
    printf("\n");
}
